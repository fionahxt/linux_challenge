# extract all mac addresses
cat hosts.real | awk '{print $3, $4}' | grep "^00:" | sed 's/ #//g' | uniq

# write IP addresses to new file
cat hosts.real | awk '{print $1}' | sed '/#/d' | uniq > ip