# extract all trade data with wTradePrice and wTradeVolume only
# steps:
# remove Regression: 
# delete all lines except ones with Publish|Type: Trade|wTradePrice|wTradeVolume
# grab 1 line before and 2  lines after Type: Trade
# fix formats:
# Add newline before each Record and delete line with divider --
# put Type: Trade onto same line as Record info
cat opra_example_regression.log | sed 's/Regression: //g' | sed -E "/(Publish|Type: Trade|wTradePrice|wTradeVolume)/!d" | grep -A 2 -B 1 "Type: Trade" | sed 's/^Record/\nRecord/g; /--/d' | sed '/^Record/N; s/\n//'